const express = require("express");
const app = express();
const bodyParser = require("body-parser");
app.listen(8080);
app.use(bodyParser.raw());
app.use(bodyParser.urlencoded({extended: true}));

app.all("/**", function(req, resp) {

    /* Prints out the basic information on any type of request
    try it out with 

    curl -X POST -d "hello world" "localhost:8080/abc/def/hij?a=2" -H "Content-Type: text/plain"

    for instance.
    Note that the bodyParser at line 5-6 is required.
    Not that the header Content-Type: [content-type] is required for the body
    to be parsed. */
    resp.send(`<pre>
Received ${req.method} on path ${req.path} with 
    body = ${JSON.stringify(req.body)}
    query = ${JSON.stringify(req.query)}
</pre>`);
    resp.end();
});

