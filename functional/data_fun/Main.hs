import qualified Control.Exception as Exc
import qualified Control.Monad as M
import QueryString
import System.Exit (exitSuccess)

callModule :: String -> IO ()
callModule str = case head str of 
    '1' -> do
        putStrLn "Enter a query string (?XX=A&YY=B)"
        getLine >>= (printUrlParams . getUrlParams)
    '2' -> undefined
    '3' -> exitSuccess
    otherwise -> putStrLn $ "Invalid option" ++ [head str]

showMenu :: IO ()
showMenu = do 
    putStrLn "Welcome to data_fun, please choose an option."
    putStrLn "1. parse html url query string"
    putStrLn "2. parse html query (headers + query, not the body)"
    putStrLn "3. exit"

main = M.forever $ do
    showMenu
    getLine >>= callModule