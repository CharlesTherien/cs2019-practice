module QueryString where

import qualified Data.HashMap.Lazy as Map
import Data.List.Split (splitOn)
import HtmlTypes

getUrlParams :: String -> QueryParams
getUrlParams str = let 
    arrayToPair :: [String] -> (String, String)
    arrayToPair x = (head x, x !! 1) 
        in  Map.fromList $                       -- Creates the map from array [(k, v)]
            map (arrayToPair . (splitOn "=")) $  -- Flatten the [[String]] array in [(k, v)]
            splitOn "&" $                        -- Split each arguments pair (still with =)
            drop 1 $                             -- Drop the ?
            dropWhile (/='?') str                -- Drop everything before the ?

printUrlParams :: QueryParams -> IO ()
printUrlParams params = mapM_ (putStrLn . show) $ Map.toList params