module HtmlTypes where

import qualified Data.HashMap.Lazy as Map
    
-- map of (name, value) representing the headers
type Headers = Map.HashMap String String

-- Map of (key, value) of encoded query params
type QueryParams = Map.HashMap String String

-- Represent the HTML method as defined in the standard
data Method = GET | POST | DELETE | PUT deriving (Show)

-- Simplified request data structure in the format "method path"
data Request = Request {
    method :: Method,
    path :: String
} deriving (Show)