import DirSize
import System.Directory (getCurrentDirectory)

main = do
    files <- getCurrentDirectory >>= allFiles
    mapM_ (putStrLn . show) files
    putStrLn "Done"
