module DirSize (allFiles) where

import System.Directory (listDirectory, makeAbsolute)
import Data.Text (pack, count)

-- Number of parent directory to visit, at most
-- 0 means no parent directory will be listed
-- 1 means 1 .. will be followed.
maxParentVisit = 1 

appendParent x = x ++ "/../"

-- allFiles :: FilePath -> IO [FilePath]
allFiles path = listFiles path 0 
    where 
        -- listFiles :: FilePath -> Int -> IO [FilePath]
        listFiles path parentCount
            | parentCount >= maxParentVisit = [(listDirectory, path)]
            | otherwise = [(listDirectory, path)] ++ listFiles (appendParent path) (parentCount + 1)